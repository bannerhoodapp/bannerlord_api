using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using bannerlord_api.Models;

namespace bannerlord_api.Data
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
            
        }

        public DbSet<User> User { get; set; }
        public DbSet<CustomPicture> CustomPicture { get; set; }
        public DbSet<CustomMarkPosition> CustomMarkPosition { get; set; }
    }
}