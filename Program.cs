using Microsoft.EntityFrameworkCore;
using Renci.SshNet;

namespace bannerlord_api
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddUserSecrets<Program>(optional: true)
                .AddEnvironmentVariables()
                .Build();
            var userId = builder.Configuration.GetValue<string>("ServerData:UserId");
            var password = builder.Configuration.GetValue<string>("ServerData:Password");
            var address = builder.Configuration.GetValue<string>("ServerData:Address");
            
            builder.Services.AddControllers();

            try
            {
                using (var client = new SshClient(address, 22, userId, password))
                {
                    Console.WriteLine("Connecting to MySQL...");
                    client.Connect();
                    if (client.IsConnected)
                    {
                        var portForwarded = new ForwardedPortLocal("127.0.0.1", 3306, "127.0.0.1", 3307);
                        client.AddForwardedPort(portForwarded);
                        portForwarded.Start();

                        var connectionString = configuration.GetConnectionString("DefaultConnection");
                        var serverVersion = new MySqlServerVersion(ServerVersion.AutoDetect(connectionString));

                        if (builder.Environment.IsDevelopment())
                        {
                            builder.Services.AddDbContext<UsersDbContext>(o => o.UseMySql(connectionString, serverVersion)
                                .LogTo(Console.WriteLine, LogLevel.Information)
                                .EnableSensitiveDataLogging()
                                .EnableDetailedErrors());
                        }
                        else if (builder.Environment.IsProduction())
                        {
                            builder.Services.AddDbContext<UsersDbContext>(o => o.UseMySql(connectionString, serverVersion));
                        }
                        var app = builder.Build();
                        app.UseAuthorization();
                        app.MapControllers();
                        app.Run();

                        client.Disconnect();
                    }
                    else
                    {
                        Console.WriteLine("Client cannot be reached...");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Custom exception: " + e.Message);
            }
        }
    }
}