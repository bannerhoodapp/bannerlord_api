using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bannerlord_api.Models
{
    public class CustomPicture
    {
        public int Id { get; set; }
        public string PictureSrc { get; set; } = string.Empty;
        public int? UserId { get; set; }
        public User? User { get; set; }
    }
}