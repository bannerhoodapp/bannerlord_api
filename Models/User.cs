using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bannerlord_api.Models
{
    [Table("users")]
    public class User
    {
        [Key]
        [Column("user_id")]
        public int Id { get; set; }
        [Column("user_email")]
        public string Email { get; set; } = string.Empty;
        [Column("user_name")]
        public string UserName { get; set; } = string.Empty;
        [Column("user_password")]
        public string Password { get; set; } = string.Empty;
    }
}