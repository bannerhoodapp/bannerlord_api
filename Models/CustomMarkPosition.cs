using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bannerlord_api.Models
{
    public class CustomMarkPosition
    {
        public int Id { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public int? CustomPictureId { get; set; }
        public CustomPicture? CustomPicture { get; set; }
    }
}